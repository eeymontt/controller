# Spectre Controller

This library is used to represent a controller that can be used with the Project Spectre firmware:

* A generic `Controller` object is defined that can be used in workflows where the specifics don't matter.
* A Spectre-specific `SpectrePID` model is contructed and can be configured according to the needs of the desired underlying model. In essence, this is just a wrapper over the `PidController` class with namespaces resolved for you.

***

## Namespace

The finite state machine library exists within the `eymonttdev::controller` namespace. For simplicity, you can include the `controller_namespace_alias.h` file to ignore namespace rules.

## Usage

The following provides examples on how to create a basic controller.

### Create a SpectrePID instance

EXAMPLE OUT OF DATE

```c++
#include "spectre_controller.h"
#include "controller_namespace_alias.h"

auto c = controller::Controller();

while (true)
{
    uint16_t time = getSystemTime_ms();
    c.update(time);
}
```

## Controllers

Currently, the `Controller` interface only supports single-input single output (SISO) controllers. These mechanisms connect to the rest of the code through pointers to an *input*, *output*, and *setpoint* variable. (Only the output variable is ever affected by the controller.) Think of this as a node with connections to a more complex controller setup where multiple SISO systems can be connected for form more complex multi-in multi-out (MIMO) setups, such as those with feedforward or cascade control loops.

## PID Controller

The `PidController` class utilizes the `Controller` interface and is thus compatible with Spectre code. It represents a Proportional–Integral–Derivative feedback controller with various features built-in, including a selection of Integrators and Derivators to choose from and several anti-windup techniques.

### Direction

Controllers act in either a forward or reverse direction as specified by the `PidDirection` enum. All controllers should be configured by the user as if acting in a forward (i.e. *direct*) manner.

When provided `PidDirection::REVERSE`, the output is multiplied by negative one **prior to** being clamped by controller output saturation limits.

### Error

...

### Tuning

Tuning of PID controllers is accomplished via tuning parameters:

* Controller gain, *K<sub>c</sub>*
* Reset time constant, *T<sub>i</sub>*
* Derivative time constant, *T<sub>d</sub>*

which can be generated from controller gains through the following relations:

$$ K_c = K_p, \quad K_i = \frac{K_p}{T_i}, \quad K_d = K_p T_d $$

These values are saved in `pidGains_t` objects which are provided by `gains.h` and utilized by `PidController` instances through the `GainManager` interface. By default these values are based in seconds. Gain scheduling based on a single parameter (i.e. *velocity*) is also supported, however custom implementations are allowed.

### Anti-Windup

Several anti-windup techniques are implemented for PID controllers. They are:

* `PidAntiWindup::None` - No anti-windup is performed. Computationally, this is the fastest algorithm to run but also the laggiest is terms of the process itself.
* `PidAntiWindup::BackCalc` - A back calculation method where the integral action is reduced by any overshoot of the total controller output.
* `PidAntiWindup::DisableAtSat` - Integration is disregarded when the internal integrator is saturated.
* ~~`PidAntiWindup::LargeError` - Integration is disregarded when the absolute error for the I-term is larger than the user-provided value of `maxIntegralErr`.~~

Only one anti-windup method may be assigned to a controller at a time.

In addition to the measures above, you can also supply a *back-calculation gain* (*K<sub>b</sub>*) for the controller to use to discharge a saturated internal integrator. This gain ranges from zero to one (i.e. 0% to 100%) on the **total value of the accumulator**, not just the overshoot! This operation runs after, i.e. independently of, the aforementioned techniques.

To disable this "discharge", set `integralDeprecPercent` to zero.

To eliminate spikes in accumulation, integration is also disabled when the instantaneous error (i.e. the change in error from one update to the next) supplied to the Integrator is too large. To disable large error compensation, set `integralMaxInstantError` to a negative value.
