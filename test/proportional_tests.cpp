#include <iostream>
#include "gtest/gtest.h"

#include "controller_namespace_alias.h"
#include "proportional_direct.h"

////////////////////////////////////////////////////////////////////////////////
//  PROPORTIONAL ACTION: DIRECT                                               //
////////////////////////////////////////////////////////////////////////////////

GTEST_TEST(ProportionalDirectImplementDirectCalc, SimpleCaseTest1)
{
    float action = DirectProportional::calculate(1, 1, 0);
    EXPECT_FLOAT_EQ(1.f, action);
}

GTEST_TEST(ProportionalDirectImplementDirectCalc, GainMultiplierTest1)
{
    float action = DirectProportional::calculate(1, 1, 0);
    EXPECT_FLOAT_EQ(1.f, action);
}

GTEST_TEST(ProportionalDirectImplementDirectCalc, GainMultiplierTest2)
{
    float action = DirectProportional::calculate(0, 1, 0);
    EXPECT_FLOAT_EQ(0, action);
}

GTEST_TEST(ProportionalDirectImplementDirectCalc, GainMultiplierTest3)
{
    float action = DirectProportional::calculate(10, 1, 0);
    EXPECT_FLOAT_EQ(10, action);
}

GTEST_TEST(ProportionalDirectImplementDirectCalc, GainMultiplierTest4)
{
    float action = DirectProportional::calculate(.1, 1, 0);
    EXPECT_FLOAT_EQ(.1f, action);
}

GTEST_TEST(ProportionalDirectImplementDirectCalc, TimestepNoEffect1)
{
    float action = DirectProportional::calculate(5, 1, 10);
    EXPECT_FLOAT_EQ(5, action);
}

GTEST_TEST(ProportionalDirectImplementDirectCalc, TimestepNoEffect2)
{
    float action = DirectProportional::calculate(5, 1, -1);
    EXPECT_FLOAT_EQ(5, action);
}

GTEST_TEST(ProportionalDirectImplementDirectCalc, TimestepNoEffect3)
{
    float action = DirectProportional::calculate(5, 1, 0.5);
    EXPECT_FLOAT_EQ(5, action);
}