#include <iostream>
#include "gtest/gtest.h"

#include "controller_namespace_alias.h"
#include "derivative_backward_euler.h"

////////////////////////////////////////////////////////////////////////////////
//  DERIVATIVE INTERFACE                                                      //
////////////////////////////////////////////////////////////////////////////////

GTEST_TEST(DerivativeInterfaceWithBackwardEuler, SetGainsToValidTest1)
{
    auto d = BackwardEulerDerivative();
    d.setControllerGain(1);
    //EXPECT_FLOAT_EQ(1, d.getControllerGain());
}

////////////////////////////////////////////////////////////////////////////////
//  DERIVATIVE ACTION: BACKWARD EULER                                         //
////////////////////////////////////////////////////////////////////////////////

GTEST_TEST(DerivativeBackwardEulerDirectCalc, SimpleCaseTest1)
{
    float action = BackwardEulerDerivative::calculate(1, 1, 0, 1, 1);
    EXPECT_FLOAT_EQ(1.f, action);
}

GTEST_TEST(DerivativeBackwardEulerDirectCalc, CorrectGainScaleTest1)
{
    float action = BackwardEulerDerivative::calculate(5, 1, 0, 1, 1);
    EXPECT_FLOAT_EQ(5.f, action);
}

GTEST_TEST(DerivativeBackwardEulerDirectCalc, CorrectGainScaleTest2)
{
    float action = BackwardEulerDerivative::calculate(0.25, 1, 0, 1, 1);
    EXPECT_FLOAT_EQ(0.25f, action);
}

GTEST_TEST(DerivativeBackwardEulerDirectCalc, NoTimestepDifferenceTest1)
{
    float previous_time = 0; 
    float current_time = 0;
    float action =
        BackwardEulerDerivative::calculate(
            1, 1, previous_time, current_time, 1
        );
    EXPECT_FLOAT_EQ(0, action);
}

GTEST_TEST(DerivativeBackwardEulerDirectCalc, NoTimestepDifferenceTest2)
{
    float previous_time = 5; 
    float current_time = 5;
    float action =
        BackwardEulerDerivative::calculate(
            1, 1, previous_time, current_time, 1
        );
    EXPECT_FLOAT_EQ(0, action);
}

GTEST_TEST(DerivativeBackwardEulerDirectCalc, NoTimestepDifferenceTest3)
{
    float previous_time = -5; 
    float current_time = -5;
    float action =
        BackwardEulerDerivative::calculate(
            1, 1, previous_time, current_time, 1
        );
    EXPECT_FLOAT_EQ(0, action);
}

GTEST_TEST(DerivativeBackwardEulerDirectCalc, NegativeTimestepTest1)
{
    float previous_time = -1; 
    float current_time = -2;
    float action =
        BackwardEulerDerivative::calculate(
            1, 1, previous_time, current_time, 1
        );
    EXPECT_FLOAT_EQ(-1, action);
}