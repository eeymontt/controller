#include <iostream>
#include "gtest/gtest.h"

#include "controller_namespace_alias.h"
#include "controller_pid.h"

////////////////////////////////////////////////////////////////////////////////
//  PID CONTROLLER: SPECTRE VERSION                                           //
////////////////////////////////////////////////////////////////////////////////



GTEST_TEST(PidControllerSpectre, BlankTest1)
{
    EXPECT_FLOAT_EQ(1, 1);
}

// @todo @eymontt
// test certain functionality for each of the settings of:
// antiwindup
// direct versus reverse action direction
// etc.