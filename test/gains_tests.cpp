#include <iostream>
#include "gtest/gtest.h"

#include "controller_namespace_alias.h"
#include "gains.h"
#include "gains_single.h"
#include "gains_multiple.h"

////////////////////////////////////////////////////////////////////////////////
//  GAINS: STRUCT OBJECT                                                      //
////////////////////////////////////////////////////////////////////////////////

GTEST_TEST(PidGainObject, DefaultValueIsJustPOfOne1)
{
    pidGains_t g;
    EXPECT_FLOAT_EQ(g.Kc, 1);
    EXPECT_FLOAT_EQ(g.Ti, 0);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(PidGainObject, CanBeAssignedByInitListZero1)
{
    pidGains_t g {};
    EXPECT_FLOAT_EQ(g.Kc, 1);
    EXPECT_FLOAT_EQ(g.Ti, 0);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(PidGainObject, CanBeAssignedByInitListZero2)
{
    pidGains_t g {0};
    EXPECT_FLOAT_EQ(g.Kc, 0);
    EXPECT_FLOAT_EQ(g.Ti, 0);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(PidGainObject, CanBeAssignedByInitList1)
{
    pidGains_t g {5, 20, 30};
    EXPECT_FLOAT_EQ(g.Kc, 5);
    EXPECT_FLOAT_EQ(g.Ti, 20);
    EXPECT_FLOAT_EQ(g.Td, 30);
}

GTEST_TEST(PidGainObject, CanBeAssignedByInitList2)
{
    pidGains_t g {1.33, 2.66, 3.55};
    EXPECT_FLOAT_EQ(g.Kc, 1.33);
    EXPECT_FLOAT_EQ(g.Ti, 2.66);
    EXPECT_FLOAT_EQ(g.Td, 3.55);
}

////////////////////////////////////////////////////////////////////////////////
//  GAINS MANAGER: SINGLE                                                     //
////////////////////////////////////////////////////////////////////////////////

GTEST_TEST(SingleGainManagerAssignment, DefaultManagerIsDefaultGains1)
{
    SingleGainManager m;
    pidGains_t g_0;

    pidGains_t g;
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Kc, g_0.Kc);
    EXPECT_FLOAT_EQ(g.Ti, g_0.Ti);
    EXPECT_FLOAT_EQ(g.Td, g_0.Td);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidPAsFloat1)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_P = 1.0f;

    m.setGains(valid_P, 0, 0);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Kc, valid_P);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidPAsFloat2)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_P = 0.0f;

    m.setGains(valid_P, 0, 0);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Kc, valid_P);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidPAsFloat3)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_P = 5.5f;

    m.setGains(valid_P, 0, 0);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Kc, valid_P);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidPAsInt1)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_P = 7;

    m.setGains(valid_P, 0, 0);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Kc, valid_P);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidIAsFloat1)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_I = 1.0f;

    m.setGains(0, valid_I, 0);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Ti, valid_I);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidIAsFloat2)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_I = 500.0f;

    m.setGains(0, valid_I, 0);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Ti, valid_I);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidDAsFloat1)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_D = 1.0f;

    m.setGains(0, 0, valid_D);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Td, valid_D);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidDAsFloat2)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_D = 100.0f;

    m.setGains(0, 0, valid_D);
    m.getGains(g);
    EXPECT_FLOAT_EQ(g.Td, valid_D);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidPidAsFloats1)
{
    SingleGainManager m;
    pidGains_t g {0};

    float valid_P = 7.f;
    float valid_I = 333.3f;
    float valid_D = 100.0f;

    m.setGains(valid_P, valid_I, valid_D);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, valid_P);
    EXPECT_FLOAT_EQ(g.Ti, valid_I);
    EXPECT_FLOAT_EQ(g.Td, valid_D);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidPidAsMixed1)
{
    SingleGainManager m;
    pidGains_t g {0};

    int valid_P = 1;
    float valid_I = 500;
    float valid_D = 100.0f;

    m.setGains(valid_P, valid_I, valid_D);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, valid_P);
    EXPECT_FLOAT_EQ(g.Ti, valid_I);
    EXPECT_FLOAT_EQ(g.Td, valid_D);
}

GTEST_TEST(SingleGainManagerAssignment, AssignValidPidAsMixed2)
{
    SingleGainManager m;
    pidGains_t g {0};

    int valid_P = 1;
    float valid_I = 0;
    float valid_D = 100.0f;

    m.setGains(valid_P, valid_I, valid_D);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, valid_P);
    EXPECT_FLOAT_EQ(g.Ti, valid_I);
    EXPECT_FLOAT_EQ(g.Td, valid_D);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidParameterP1)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(-1, 0, 0);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidParameterP2)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(-100., 0, 0);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidParameterI1)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(0, -1, 0);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Ti, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidParameterI2)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(0, -100., 0);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Ti, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidParameterD1)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(0, 0, -1);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidParameterD2)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(0, 0, -100.);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidParameters1)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(-55555, -7, -100.);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, 0);
    EXPECT_FLOAT_EQ(g.Ti, 0);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidMixed1)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(-55555, 7, -100.);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, 0);
    EXPECT_FLOAT_EQ(g.Ti, 7);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(SingleGainManagerAssignment, AssignInvalidMixed2)
{
    SingleGainManager m;
    pidGains_t g {99, 99, 99};

    m.setGains(55555, 7, -100.);
    m.getGains(g);
    
    EXPECT_FLOAT_EQ(g.Kc, 55555);
    EXPECT_FLOAT_EQ(g.Ti, 7);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

////////////////////////////////////////////////////////////////////////////////
//  GAINS MANAGER: MULTIPLE                                                   //
////////////////////////////////////////////////////////////////////////////////

class MultiGainDirectRetrieval : public ::testing::Test
{
protected:
    MultiGainDirectRetrieval()
        : pidScheduleTable{
            {10.f, {5.f,  0.01f,  0.1f}},
            {20.f, {1.1f, 0.15f, 0.05f}},
            {30.f, {1.2f, 0.2f,  0.1f}}}
        , velocity_outside{0}
    {}

    virtual void SetUp()
    {      
        auto numRows = (sizeof(pidScheduleTable) / sizeof(pidScheduleTable[0]));
        mgm.assignTable(pidScheduleTable, numRows);
        mgm.assignVelocityRef(&velocity_outside);
    }

    virtual void TearDown()
    {
        // nothing needed...
    }

    pidTableRow_t pidScheduleTable[3];
    MultiGainManager mgm;
    float velocity_outside;
};

TEST_F(MultiGainDirectRetrieval, InMiddleOfRange1)
{
    pidGains_t g;
    velocity_outside = 5;
    mgm.getGains(g);

    EXPECT_FLOAT_EQ(g.Kc, 5);
    EXPECT_FLOAT_EQ(g.Ti, 0.01);
    EXPECT_FLOAT_EQ(g.Td, 0.1);
}

TEST_F(MultiGainDirectRetrieval, InMiddleOfRange2)
{
    pidGains_t g;
    velocity_outside = 15;
    mgm.getGains(g);

    EXPECT_FLOAT_EQ(g.Kc, 1.1);
    EXPECT_FLOAT_EQ(g.Ti, .15);
    EXPECT_FLOAT_EQ(g.Td, .05);
}

TEST_F(MultiGainDirectRetrieval, InMiddleOfRange3)
{
    pidGains_t g;
    velocity_outside = 25;
    mgm.getGains(g);

    EXPECT_FLOAT_EQ(g.Kc, 1.2);
    EXPECT_FLOAT_EQ(g.Ti, .2);
    EXPECT_FLOAT_EQ(g.Td, .1);
}

TEST_F(MultiGainDirectRetrieval, BelowValidVelocity1)
{
    pidGains_t g;
    velocity_outside = -1;
    mgm.getGains(g);

    EXPECT_FLOAT_EQ(g.Kc, 0);
    EXPECT_FLOAT_EQ(g.Ti, 0);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

TEST_F(MultiGainDirectRetrieval, AtZeroVelocity1)
{
    pidGains_t g;
    velocity_outside = 0.;
    mgm.getGains(g);

    EXPECT_FLOAT_EQ(g.Kc, 5);
    EXPECT_FLOAT_EQ(g.Ti, 0.01);
    EXPECT_FLOAT_EQ(g.Td, 0.1);
}

TEST_F(MultiGainDirectRetrieval, AtMaxVelocity1)
{
    pidGains_t g;
    velocity_outside = 30;
    mgm.getGains(g);

    EXPECT_FLOAT_EQ(g.Kc, 1.2);
    EXPECT_FLOAT_EQ(g.Ti, .2);
    EXPECT_FLOAT_EQ(g.Td, .1);
}

TEST_F(MultiGainDirectRetrieval, AboveValidVelocity1)
{
    pidGains_t g;
    velocity_outside = 35;
    mgm.getGains(g);

    EXPECT_FLOAT_EQ(g.Kc, 0);
    EXPECT_FLOAT_EQ(g.Ti, 0);
    EXPECT_FLOAT_EQ(g.Td, 0);
}

GTEST_TEST(MultiGainPolyRetrieval, ReferenceAsBaseClass1)
{
    ASSERT_TRUE(true);
}