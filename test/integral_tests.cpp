#include <iostream>
#include "gtest/gtest.h"

#include "controller_namespace_alias.h"
#include "integral_backward_euler.h"
#include "integral_tustin.h"

////////////////////////////////////////////////////////////////////////////////
//  INTEGRAL ACTION: INTERFACE                                                //
////////////////////////////////////////////////////////////////////////////////

GTEST_TEST(IntegralInterface, BackwardEulerDefaultAction1)
{
    BackwardEulerIntegral i;
    EXPECT_FLOAT_EQ(0, i.action());
}

GTEST_TEST(IntegralInterface, BackwardEulerAssignAction1)
{
    BackwardEulerIntegral i;
    i.setIntegralAction(100.f);
    EXPECT_FLOAT_EQ(100, i.action());
}

GTEST_TEST(IntegralInterface, BackwardEulerAssignAction2)
{
    BackwardEulerIntegral i;
    i.setIntegralAction(-5);
    EXPECT_FLOAT_EQ(-5, i.action());
}

////////////////////////////////////////////////////////////////////////////////
//  INTEGRAL ACTION: BACKWARD EULER                                           //
////////////////////////////////////////////////////////////////////////////////

GTEST_TEST(IntegralBackwardEulerDirectCalc, SimpleCaseTest1)
{
    float action = BackwardEulerIntegral::calculate(1, 1, 0, 1, 1);
    EXPECT_FLOAT_EQ(1.f, action);
}

GTEST_TEST(IntegralBackwardEulerDirectCalc, NoPreviousAction1)
{
    float previous = 0; 
    float action =
        BackwardEulerIntegral::calculate(
            5.f, 1.f, previous, 1, 1
        );
    EXPECT_FLOAT_EQ(5, action);
}

GTEST_TEST(IntegralBackwardEulerDirectCalc, NoPreviousAction2)
{
    float previous = 0; 
    float action =
        BackwardEulerIntegral::calculate(
            10.f, 1.f, previous, 3, 1
        );
    EXPECT_FLOAT_EQ(30, action);
}

GTEST_TEST(IntegralBackwardEulerDirectCalc, NoPreviousAction3)
{
    float previous = 0; 
    float action =
        BackwardEulerIntegral::calculate(
            20.f, 1.f, previous, 1, 2
        );
    EXPECT_FLOAT_EQ(40, action);
}

GTEST_TEST(IntegralBackwardEulerDirectCalc, SumToPrevAction1)
{
    float previous = 10; 
    float action =
        BackwardEulerIntegral::calculate(
            5.f, 1.f, previous, 1, 1
        );
    EXPECT_FLOAT_EQ(15, action);
}

GTEST_TEST(IntegralBackwardEulerDirectCalc, SumToPrevAction2)
{
    float previous = 30; 
    float action =
        BackwardEulerIntegral::calculate(
            20.f, 1.f, previous, 1, 2
        );
    EXPECT_FLOAT_EQ(70, action);
}