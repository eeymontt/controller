/** @file */

#ifndef EYMDEV__CONTROLLER_DERIV_LOW_PASS_FILTER__H
#define EYMDEV__CONTROLLER_DERIV_LOW_PASS_FILTER__H

#include "derivative_action.h"
#include <stdint.h>

namespace eymonttdev {
namespace controller {

/** Derivative action with a Low-pass filter */
class LowPassDerivative : public DerivativeFunction
{
    public:
        uint8_t N; // Filter coefficient.
        inline LowPassDerivative(uint8_t N) : N(N){}
        virtual void updateAction(const controllerTerm_t& error, float dt_s) override;

        /**
         * @brief Calculate derivative action with LPF applied.
         * 
         * @param[in] Kc Controller gain.
         * @param[in] Td Derivative time constant.
         * @param[in] deriv_prev Previous derivative action.
         * @param[in] term_prev Previously calculated error value.
         * @param[in] term_curr Current error value.
         * @param[in] dt Delta time in [s] between previous calculation and current time.
         * @param[in] N Derivative filter coefficient.
         * 
         * @return Contribution from derivative action.
         */
        static float calculate(float Kc, float Td, float deriv_prev, float term_prev, float term_curr, float dt, uint8_t N);
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_DERIV_LOW_PASS_FILTER__H */ 