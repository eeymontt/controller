/** @file */

#ifndef EYMDEV__DERIVATIVE_ACTION_INTF__H
#define EYMDEV__DERIVATIVE_ACTION_INTF__H

#include "controller_term.h"

namespace eymonttdev {
namespace controller {

class DerivativeFunction
{
    using derivativeTerm_t = controllerTerm_t;
    public:
        DerivativeFunction() { deriv_term_.reset(); }

        /**
         * @brief Provide time constants.
         * @param[in] Kc Controller gain.
         * @param[in] Td Derivative time constant.
         */
        void setDerivativeGains(float Kc, float Td)
        {
            setControllerGain(Kc);
            setDerivativeTimeConstant(Td);
        }

        inline void setControllerGain(float Kc)
        {
            // assign absolute value
            K_ = Kc * ((Kc > 0) - (Kc < 0));
        }
        inline void setDerivativeTimeConstant(float Td)
        {
            // assign absolute value
            Td_ = Td * ((Td > 0) - (Td < 0));
        }

        /**
         * @brief Update the derivative function.
         * @param[in] error Reference to the 'error on' term.
         * @param[in] dt_s Delta time in [s] between error values.
         */
        virtual void updateAction(const controllerTerm_t& error, float dt_s) = 0;

        /**
         * @brief Get the current derivative action.
         * @return Absolute contribution from the derivative term.
         * @warning When applying derivative action, recall that it should act
         * with opposite sign to the rest of the PID response.
         */
        inline float action() const { return deriv_term_.current; }

    protected:
        float K_;
        float Td_;

        /* Derivative action */
        derivativeTerm_t deriv_term_;
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__DERIVATIVE_ACTION_INTF__H */ 