/** @file */

#ifndef EYMDEV__CONTROLLER_DERIV_EWMA__H
#define EYMDEV__CONTROLLER_DERIV_EWMA__H

#include "derivative_action.h"

namespace eymonttdev {
namespace controller {

/** Derivative action with an EWMA filter. */
class EwmaDerivative : public DerivativeFunction
{
    public:
        float alpha; // Filter weight for EWMA; alpha ∈ [0.0, 1.0)
        inline EwmaDerivative(float alpha = 0.95f) : alpha(alpha){}
        virtual void updateAction(const controllerTerm_t& error, float dt_s) override;

        /**
         * @brief Calculate derivative action with an EWMA filter applied.
         * 
         * @param[in] Kc Controller gain.
         * @param[in] Td Derivative time constant.
         * @param[in] deriv_prev Previous derivative action.
         * @param[in] term_prev Previously calculated error value.
         * @param[in] term_curr Current error value.
         * @param[in] dt Delta time in [s] between previous calculation and current time.
         * @param[in] alpha EWMA weight.
         * 
         * @return Contribution from derivative action.
         */
        static float calculate(float Kc, float Td, float deriv_prev, float term_prev, float term_curr, float dt, float alpha);
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_DERIV_EWMA__H */ 