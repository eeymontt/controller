/** @file */

#ifndef EYMDEV__PROPORTIONAL_ACTION_INTF__H
#define EYMDEV__PROPORTIONAL_ACTION_INTF__H

#include "controller_term.h"

namespace eymonttdev {
namespace controller {

class ProportionFunction
{
    using proportionalTerm_t = controllerTerm_t;
    public:
        ProportionFunction() { prop_term_.reset(); }

        /**
         * @brief Provide controller gain constant.
         * @param[in] Kc Controller gain.
         */
        void setControllerGain(float Kc) { if (Kc >= 0.f) K_ = Kc; }

        /**
         * @brief Update the proportional function.
         * @param[in] error Reference to the 'error on' term.
         * @param[in] dt_s Delta time in [s] between error values.
         */
        virtual void updateAction(const controllerTerm_t& error, float dt_s) = 0;

        /**
         * @brief Get the current proportional action.
         * @return Absolute contribution from the proportional term.
         */
        float action() const { return prop_term_.current; }

    protected:
        float K_;
        proportionalTerm_t prop_term_;
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__PROPORTIONAL_ACTION_INTF__H */ 