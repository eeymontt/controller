/** @file */

#ifndef EYMDEV__CONTROLLER_GAIN_MANAGER_SINGLE__H
#define EYMDEV__CONTROLLER_GAIN_MANAGER_SINGLE__H

#include "gains.h"

namespace eymonttdev {
namespace controller {

/** Validation and storage object for a single set of controller gains. */
class SingleGainManager : public GainManager
{
    public:
        /**
         * @brief Assign tuning parameters.
         * 
         * @param[in] Kc Controller gain, [(% of CO signal)/(% of PV signal)]
         * @param[in] Ti Reset time constant, [seconds / rep.].
         * @param[in] Td Rate time constant, [seconds].
         * 
         * @note Only accepts positive values; negative values will be regarded
         * as zero.
         */
        void setGains(float Kc, float Ti, float Td);

        virtual void getGains(pidGains_t& gains) const override;
        virtual ~SingleGainManager() = default;
    protected:
        pidGains_t gains_;
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_GAIN_MANAGER_SINGLE__H */