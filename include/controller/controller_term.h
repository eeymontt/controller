/** @file */

#ifndef EYMDEV__CONTROLLER_TERM__H
#define EYMDEV__CONTROLLER_TERM__H

#include <iostream>

namespace eymonttdev {
namespace controller {

/** Internal controller variable with history. */
struct controllerTerm_t
{
    float current;      /**< Value during current iteration. */
    float previous;     /**< Value from previous iteration. */

    /** Clear the variable state. */
    void reset()
    {
        current = 0.f;
        previous = 0.f;
    }
};

std::ostream& operator<<(std::ostream& os, const controllerTerm_t& term);

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_TERM__H */