/** @file */

#ifndef EYMDEV__CONTROLLER_INTERFACE__H
#define EYMDEV__CONTROLLER_INTERFACE__H

#include <stdint.h>
#include "controller_term.h"

namespace eymonttdev {
namespace controller {

/**
 * @brief Abstract class that all SISO controllers derive from.
 * 
 * This interface declares the API through which single-input single-output
 * (SISO) controllers should be interacted with by flight code.
 */
class Controller
{
public:
    /**
     * @brief Connects the controller to the rest of the system.
     * 
     * Provide references to the controller that correspond with the dynamic
     * system that is to be controlled.
     * 
     * @param[in] input Reference to the measured process variable (PV).
     * @param[in] output Reference to the control value (CV) or (CO).
     * @param[in] setpoint Reference to the system setpoint (SP).
     * 
     * @note The names of the terms may differ depending on the context; in a 
     * traditional PID diagram, these values can also be referred to as: the
     * input is y(t), th eoutput is u(t), and the setpoint is r(t).
     */
    void setConnections(float *input, float *output, float *setpoint)
    {
        if (input) this->input = input;
        if (output) this->output = output;
        if (setpoint) this->setpoint = setpoint;
    }

    /**
     * @brief Updates the controller output.
     * @param[in] time_us The current system time, in [us].
     */
    virtual void update(uint16_t time_us) = 0;

    /**
     * @brief Resets the controller.
     * @returns A boolean indicating if the reset was successful (true) or
     * not (false).
     */
    virtual bool reset() = 0;

    /**
     * @brief Retrieves controller error.
     * @return The current error.
     */
    float error() { return sys_error_.current; }

    // default destructor
    virtual ~Controller(){};

protected:
    controllerTerm_t sys_error_;    /**< Error value. */

    /* Connections to outside world */

    float *input = nullptr;
    float *output = nullptr;
    float *setpoint = nullptr;
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_INTERFACE__H */