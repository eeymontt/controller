/** @file */

#ifndef EYMDEV__CONTROLLER_GAIN_INTERFACE__H
#define EYMDEV__CONTROLLER_GAIN_INTERFACE__H

#include <stdint.h>

namespace eymonttdev {
namespace controller {

/**
 * Unique set of tuning parameters.
 * @warning Default value is a proportional controller with gain of one.
 */
struct pidGains_t
{
    float Kc {1.f}; /**< Proportional gain. */
    float Ti {0.f}; /**< Reset time constant ("integral gain"). */
    float Td {0.f}; /**< Derivative time constant ("derivative gain"). */
};

/** Interface that manages storage & retrieval of PID controller gains. */
class GainManager
{
    public:
        /**
         * @brief Retrieve controller gains.
         * @param[out] gains Object to be filled with tuning parameters.
         * @note Be aware of the format through which values are provided, i.e
         * as to whether they are term gains or tuning parameters.
         */
        virtual void getGains(pidGains_t& gains) const = 0;

        // default destructor
        virtual ~GainManager(){};
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_GAIN_INTERFACE__H */