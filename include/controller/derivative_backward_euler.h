/** @file */

#ifndef EYMDEV__CONTROLLER_DERIV_BACKWARD_EULER__H
#define EYMDEV__CONTROLLER_DERIV_BACKWARD_EULER__H

#include "derivative_action.h"

namespace eymonttdev {
namespace controller {

/** Simple derivative action */
class BackwardEulerDerivative : public DerivativeFunction
{
    /*
    Arguably the simplest (meaningful) discretization is found through the
    Backward Euler method which is essentially a difference quotient with extra
    steps.
    */
    public:
        virtual void updateAction(const controllerTerm_t& error, float dt_s) override;

        /**
         * @brief Calculate derivative action via Backward Euler method.
         * 
         * @param[in] Kc Controller gain.
         * @param[in] Td Derivative time constant.
         * @param[in] term_prev Previously calculated error value.
         * @param[in] term_curr Current error value.
         * @param[in] dt Delta time in [s] between previous calculation and current time.
         * 
         * @return Contribution from derivative action.
         */
        static float calculate(float Kc, float Td, float term_prev, float term_curr, float dt);
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_DERIV_BACKWARD_EULER__H */ 