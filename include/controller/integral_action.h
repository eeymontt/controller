/** @file */

#ifndef EYMDEV__INTEGRAL_ACTION_INTF__H
#define EYMDEV__INTEGRAL_ACTION_INTF__H

#include "controller_term.h"

namespace eymonttdev {
namespace controller {

class IntegralFunction
{
    using integralTerm_t = controllerTerm_t;
    public:
        IntegralFunction() { integ_term_.reset(); }

        /**
         * @brief Provide time constants.
         * @param[in] Kc Controller gain.
         * @param[in] Ti Reset (a.k.a. integral) time constant.
         */
        void setIntegralGains(float Kc, float Ti)
        {
            setControllerGain(Kc);
            setResetTimeConstant(Ti);
        }

        inline void setControllerGain(float Kc)
        {
            // assign absolute value
            K_ = Kc * ((Kc > 0) - (Kc < 0));
        }
        inline void setResetTimeConstant(float Ti)
        {
            // assign absolute value
            Ti_ = Ti * ((Ti > 0) - (Ti < 0));
        }

        /**
         * @brief Set the Integral action value. 
         * @param[in] actionValue New value.
         * 
         * @note This method is provided for use in anti-windup methods where
         * an external [to the Integrator] connection to the controller is
         * required.
         */
        inline void reassignAction(float actionValue)
        {
            integ_term_.current = actionValue;
        }

        /**
         * @brief Update the integral function.
         * @param[in] error Reference to the 'error on' term.
         * @param[in] dt_s Delta time in [s] between error values.
         */
        virtual void updateAction(const controllerTerm_t& error, float dt_s) = 0;

        /**
         * @brief Get the current integral action.
         * @return Absolute contribution from the integral term.
         */
        inline float action() const { return integ_term_.current; }

    protected:
        float K_;
        float Ti_;

        /* Integral action */
        integralTerm_t integ_term_;
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__INTEGRAL_ACTION_INTF__H */ 