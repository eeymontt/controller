/** @file */

#ifndef EYMDEV__CONTROLLER_GAIN_MANAGER_MULTI__H
#define EYMDEV__CONTROLLER_GAIN_MANAGER_MULTI__H

#include "gains.h"

namespace eymonttdev {
namespace controller {

struct pidTableRow_t
{
    float maxVelocity;
    pidGains_t entry;
};

/** Validation and storage object with a lookup table for gain scheduling. */
class MultiGainManager  : public GainManager
{
    public:
        void assignTable(pidTableRow_t *table, uint8_t numRows);
        void assignVelocityRef(float *vel);
        virtual void getGains(pidGains_t& gains) const override;

        virtual ~MultiGainManager() = default;
    protected:
        float *_vel;
        pidTableRow_t *_table;
        unsigned int _numRows;

        /**
         * @brief Look up gains corresponding to a specific range of velocities.
         * 
         * This is an internal function called by `MultiGainManager::getGains`.
         * The desired velocity is populated by the reference provided by the
         * class instance.
         * 
         * @param[in] vel Velocity that indicates max value for a selected
         * bucket, in [m/s].
         * @param[out] gains Object to be filled with gain values.
         */
        void getGainsAtVelocity_(
            pidGains_t &gains, pidTableRow_t *tab, unsigned int numRows, float vel
        ) const;
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_GAIN_MANAGER_MULTI__H */