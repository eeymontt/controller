/** @file */

#ifndef EYMDEV__CONTROLLER_PID__H
#define EYMDEV__CONTROLLER_PID__H

#include "controller.h"

#include "gains.h"
#include "proportional_action.h"
#include "integral_action.h"
#include "derivative_action.h"

#include <stdint.h>

namespace eymonttdev {
namespace controller {

/** Type of value used for error calculation. */
enum class PidErrorValueType
{
    OnMeasurement,  /**< Change in measurement. */
    OnSetpoint,     /**< Difference between SP and PV. */
};

/** Method of integral anti-windup to be employed. */
enum class PidAntiWindup
{
    None,           /**< No anti-windup. */
    BackCalc,       /**< Back-calculation method. */
    DisableAtSat,   /**< Integration ignored at saturation. */
};

/** Represent direction of controller action. */
enum class PidDirection: int8_t
{
    Direct  =  1,
    Reverse = -1
};

/** Settings to configure a PID controller. */
struct settingsForPid_t
{
    // Direction of action
    PidDirection direction;

    PidErrorValueType pErrorType;
    PidErrorValueType dErrorType;

    /*
    Adjust the sensitivity of error fed to the P-term.

    It is desireable in some cases to square the proportional term error in
    order to increase the sensitivity of the P-term. True = enabled; false
    otherwise.
    */
    bool pErrorSquared {false};

    PidAntiWindup antiWindup;
    
    // Maximum error for which integral term accumulates on update; -1 = ignored.
    float integralMaxInstantError;

    // Saturation limit; -1 = no limit.
    float integralMaxValue;

    // https://en.wikipedia.org/wiki/Integral_windup
    bool zeroIntegralOnErrorSignChange;

    /*
    Decrease a saturated integral accumulator by a given percentage.
    
    A back-calculation gain (Kb) value of [0 to 1) translates to a percentage
    decrease of total accumulator value (not just overshoot) by 0% to 100%
    respectively.

    @todo define whether it can reduce to below sat. limits or not
    */
    /*
    Depreciation percentage for saturated "I" term.

    Might be referred to as the back-calculation gain, Kb.

    @url https://www.scs-europe.net/services/ecms2006/ecms2006%20pdf/107-ind.pdf
    Rule-of-thumb: Error signal fed back to integrator through gain 1/Tt,
                   where Tt ~ sqrt(Ti*Td)
                   For manual tuning, start at Ti, and adjust up/down as desired

    @url https://folk.ntnu.no/skoge/prost/proceedings/PID-2018/0061.PDF
    "adds an extra feedback signal to the input
    of the integrator, which is composed of the error between
    the output signal of the controller and the signal that is
    applied to the plant multiplied by a constant gain, known
    as tracking time parameter."

    Positive values from 0 to 1 correspond to a 0% to 100% decrease in the
    value of integral accumulator. Negative values are invalid.
    */
    float integralDeprecPercent;

    // Setpoint-weighting for 2-DOF controllers (as fractional scalar 0 to 1)
    float pTermWeight;  // b or beta, in literature
    float dTermWeight;  // c or gamma, in lit.

    // Output limits
    float outputMax; /**< Signed upper limit for controller output. */
    float outputMin; /**< Signed lower limit for controller output. */
};

/** Proportional-Integral-Derivative (PID) controller with tunable properties
and configurable action term calculations. */
class PidController : public Controller
{

using PFunct = ProportionFunction;
using IFunct = IntegralFunction;
using DFunct = DerivativeFunction;

public:
    settingsForPid_t settings;

    // constructors
    PidController(PFunct& prop, IFunct& integ, DFunct& deriv, const GainManager& manager);
    PidController(PFunct& prop, IFunct& integ, DFunct& deriv, const GainManager& manager,
        settingsForPid_t external_settings
    );

    virtual void update(uint16_t time_ms) override;
    virtual bool reset() override;
    
protected:
    
    PFunct& p_term_;
    IFunct& i_term_;
    DFunct& d_term_;
    const GainManager& gains_;

    uint16_t time_last_update_ms_;

    /** Local copy of measured process variable. */
    controllerTerm_t y_;
    float y_0;  // initial input value; reset with SP changes

    /** Local copy of setpoint variable. */
    controllerTerm_t r_;

    controllerTerm_t error_for_p_;   /**< Error term supplied to proportional action. */
    controllerTerm_t error_for_d_;   /**< Error term supplied to derivative action. */
    // Note: integral action uses the un-weighted system error

    /**
     * @brief Calculate current error value.
     * 
     * Calculate the current error value based on the difference between two
     * terms, where the first term can be weighted. The first term is generally
     * the system setpoint.
     * 
     * With the definition of a 2-DOF PID controller, the weight is a tunable
     * parameter. Weighted errors are typically only given to the proportional
     * and derivative terms.
     * 
     * @param[in] lho Left-hand operand, typically the setpoint.
     * @param[in] rho Right-hand operand, typically meas. process variable.
     * @param[in] weight Setpoint weight, w ∈ [0.0, 1.0]. Default = 1.
     * 
     * @note A @p weight of 1 is equivalent to an un-weighted setpoint.
     */
    inline float calc_error_(float lho, float rho, float weight = 1.f)
    {
        return lho*weight - rho;
    }
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_PID__H */