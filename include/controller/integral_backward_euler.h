/** @file */

#ifndef EYMDEV__CONTROLLER_INTEGRAL_BACKWARD_EULER__H
#define EYMDEV__CONTROLLER_INTEGRAL_BACKWARD_EULER__H

#include "integral_action.h"

namespace eymonttdev {
namespace controller {

/** Simple integral action using backwards difference. */
class BackwardEulerIntegral : public IntegralFunction
{
    public:
        virtual void updateAction(const controllerTerm_t& error, float dt_s) override;

        /**
         * @brief Calculate integral action via Backward Euler method.
         * 
         * Approximation of integral term via backward difference.
         * 
         * @param[in] Kc Controller gain.
         * @param[in] Ti Reset time constant.
         * @param[in] integ_prev Previous integration term ("integral accumulator").
         * @param[in] term_curr Current error value.
         * @param[in] dt Delta time in [s] between previous calculation and current time.
         * 
         * @return Contribution from integral action.
         */
        static float calculate(float Kc, float Ti, float integ_prev, float term_curr, float dt);
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_INTEGRAL_BACKWARD_EULER__H */ 