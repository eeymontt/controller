/** @file */

#ifndef EYMDEV__CONTROLLER_DERIVATIVE_BACKWARD_EULER__H
#define EYMDEV__CONTROLLER_DERIVATIVE_BACKWARD_EULER__H

#include "proportional_action.h"

namespace eymonttdev {
namespace controller {

/** Simple proportional action */
class DirectProportional : public ProportionFunction
{
    public:
        
        virtual void updateAction(const controllerTerm_t& error, float dt_s) override;

        /**
         * @brief Calculate directly-applied proportional action.
         * 
         * @param[in] Kc Controller gain.
         * @param[in] term_curr Current error value.
         * @param[in] dt Delta time in [s] between previous calculation and current time.
         * 
         * @return Contribution from proportional action.
         */
        static float calculate(float Kc, float term_curr, float dt);
};

} // namespace controller
} // namespace eymonttdev

#endif /* EYMDEV__CONTROLLER_DERIVATIVE_BACKWARD_EULER__H */ 