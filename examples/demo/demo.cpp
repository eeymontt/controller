#include <iostream>
#include <fstream>
#include <stdint.h>
#include <chrono>

#include "controller_pid.h"
#include "controller_namespace_alias.h"

// specific implementation
#include "proportional_direct.h"
#include "integral_backward_euler.h"
#include "derivative_backward_euler.h"
#include "gains_single.h"

static PidController* get_spectre_pid_controller();
static void update_controller(Controller& c);

static uint16_t getTimeInMs(); // get current (relative) time
const uint16_t time_start_ms = getTimeInMs();

int main(int argc, char *argv[])
{
    std::ofstream myFile("demo.txt", std::ios::out | std::ios::app);
    if (!myFile.is_open()) return EXIT_FAILURE;

    float input = 0.f;
    float output = 0.f;
    float setpoint = 0.f;

    auto pid = get_spectre_pid_controller();
    pid->setConnections(&input, &output, &setpoint);

    bool running = true;
    while(running)
    {
        update_controller(*pid);
        //myFile << "Updated!\n";

        if (getTimeInMs() > 5000) running = false;
        if (getTimeInMs() > 2500) setpoint = 1.0f;
    }

    myFile.close();
    std::cout << "Program done." << std::endl;

    return EXIT_SUCCESS;
}

static PidController* get_spectre_pid_controller()
{
    static auto p = DirectProportional();
    static auto i = BackwardEulerIntegral();
    static auto d = BackwardEulerDerivative();
    static auto g = SingleGainManager();
    static auto pid = PidController(p, i, d, g);
    return &pid;
}

static void update_controller(Controller &c)
{
    c.update(getTimeInMs());
}

static uint16_t getTimeInMs()
{
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;
    using std::chrono::system_clock;
    return
        (duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count()
        -  time_start_ms);
}