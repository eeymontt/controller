/** @file */

#include <iostream>
#include <stdint.h>
#include <chrono>

#include "core.h"
#include "spectre_controller.h"
#include "integral_tustin.h"
#include "derivative_lpf.h"
#include "multi_gain_manager.h"

// forward declarations
static uint16_t getTimeInMs(); // get current (relative) time

using std::cout, std::endl;

int main(int argc, char* argv[])
{
    float velocity;
    float input, output, setpoint;

    cout << "Let's make a PID controller!" << endl;

    // create settings
    spectre::settingsForPID_t pid_settings;
    pid_settings.direction = spectre::PIDDirection::DIRECT;
    pid_settings.proportional = spectre::PIDValueType::ON_ERROR;
    pid_settings.derivative = spectre::PIDValueType::ON_MEASUREMENT;
    pid_settings.propErrorSquared = false;
    pid_settings.antiWindup = spectre::PIDAntiWindup::BACK_CALC;
    pid_settings.maxIntegralErr = 50.f;
    pid_settings.satIntegralDeprecPercent = 0.1f;
    pid_settings.spWeightProp = 1.f;
    pid_settings.spWeightDeriv = 1.f;
    pid_settings.outputMax = 11.f;
    pid_settings.outputMin = -11.f;
    
    spectre::IntegrationViaTustins integrator;
    spectre::IntegralAction *pid_integrator = &integrator;
  
    spectre::DerivativeWithFilter differentiator(8U);
    spectre::DerivativeAction *pid_differentiator = &differentiator;

    // create gain manager
    spectre::GainManager *gainManager;
    spectre::pidTableRow_t pidScheduleTable[] = {
        {10.f, {5.f,  0.01f,  0.1f}},
        {20.f, {1.1f, 0.15f, 0.05f}},
        {30.f, {1.2f, 0.2f,  0.1f}}
    };
    auto numRows = (sizeof(pidScheduleTable) / sizeof(pidScheduleTable[0]));

    spectre::MultiGainManager mgm;
    mgm.assignTable(pidScheduleTable, numRows);
    mgm.assignVelocityRef(&velocity);
    gainManager = &mgm;

    // use controller pointer
    spectre::Controller* controller;

    spectre::SpectrePID pid;
    pid.init(&pid_settings);
    pid.assignIntegrator(pid_integrator);
    pid.assignDifferentiator(pid_differentiator);
    pid.assignGainManager(gainManager);

    controller = &pid;
    controller->setConnections(&input, &output, &setpoint);

    std::cout << "Let's update!" << std::endl;
    controller->update(uint16_t(0));

    return 0;
}

static uint16_t getTimeInMs()
{
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;
    using std::chrono::system_clock;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}