#include "controller_term.h"

namespace eymonttdev {
namespace controller {

std::ostream& operator<<(std::ostream& os, const controllerTerm_t& term)
{
    return os << "<prev: " << term.previous << ", "
              << "curr: " << term.current << ">";
}

} // namespace controller
} // namespace eymonttdev