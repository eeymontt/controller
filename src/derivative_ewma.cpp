#include "derivative_ewma.h"

namespace eymonttdev {
namespace controller {

void EwmaDerivative::updateAction(const controllerTerm_t& error, float dt_s)
{
    deriv_term_.previous = deriv_term_.current;
    float action = calculate(K_, Td_, deriv_term_.previous, error.previous, error.current, dt_s, this->alpha);
    deriv_term_.current = action;
}

float EwmaDerivative::calculate(
    float Kc, float Td,
    float deriv_prev,
    float term_prev, float term_curr,
    float dt,
    float alpha)
{
    return Kc*Td*(1 + alpha)/dt*(term_curr - term_prev) - alpha*deriv_prev;
}

} // namespace controller
} // namespace eymonttdev
