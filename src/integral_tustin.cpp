#include "integral_tustin.h"

namespace eymonttdev {
namespace controller {

void TustinIntegral::updateAction(const controllerTerm_t& error, float dt_s)
{
    integ_term_.previous = integ_term_.current;
    float action = calculate(K_, Ti_, integ_term_.previous, error.previous, error.current, dt_s);
    integ_term_.current = action;
}

float TustinIntegral::calculate(float Kc, float Ti, float integ_prev, float term_prev, float term_curr, float dt)
{
    /*
    To calculate the integral action:
    
        IntegAction{k} = IntegAction{k-1} + [(Kc/Ti)*Ts] * (x{k} + x{k-1})/2
    */
    return integ_prev + (Kc*dt)/Ti * (term_curr+term_prev)/2.f;
}

} // namespace controller
} // namespace eymonttdev
