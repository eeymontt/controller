#include "derivative_lpf.h"

namespace eymonttdev {
namespace controller {

void LowPassDerivative::updateAction(const controllerTerm_t& error, float dt_s)
{
    deriv_term_.previous = deriv_term_.current;
    float action = calculate(K_, Td_, deriv_term_.previous, error.previous, error.current, dt_s, this->N);
    deriv_term_.current = action;
}

float LowPassDerivative::calculate(
    float Kc, float Td,
    float deriv_prev,
    float term_prev, float term_curr,
    float dt,
    uint8_t N)
{
    return Td/(Td + N*dt)*deriv_prev - (Kc*Td*N)/(Td + N*dt)*(term_curr-term_prev);
}

} // namespace controller
} // namespace eymonttdev
