#include "derivative_backward_euler.h"

namespace eymonttdev {
namespace controller {

void BackwardEulerDerivative::updateAction(const controllerTerm_t& error, float dt_s)
{
    float action = calculate(K_, Td_, error.previous, error.current, dt_s);
    deriv_term_.previous = deriv_term_.current;
    deriv_term_.current = action;
}

float BackwardEulerDerivative::calculate(float Kc, float Td, float term_prev, float term_curr, float dt)
{
    /*
    Backward Euler Method uses the following recurrance relation:
    
        deriv{k} = (x{k} - x{k-1}) / Ts
    
    To generate the derivative action:
    
        DerivAction{k} = Kc * deriv{k} = Kc[(x{k} - x{k-1}) / Ts]
    */
    return Kc*Td*(term_curr - term_prev)/dt;
}

} // namespace controller
} // namespace eymonttdev
