#include "integral_backward_euler.h"

namespace eymonttdev {
namespace controller {

void BackwardEulerIntegral::updateAction(const controllerTerm_t& error, float dt_s)
{
    integ_term_.previous = integ_term_.current;
    float action = calculate(K_, Ti_, integ_term_.previous, error.current, dt_s);
    integ_term_.current = action;
}

float BackwardEulerIntegral::calculate(float Kc, float Ti, float integ_prev, float term_curr, float dt)
{
    /*
    To calculate the integral action:
    
        IntegAction{k} = IntegAction{k-1} + [(Kc/Ti)*Ts] * x{k}
    */
    return integ_prev + (Kc*dt)/Ti*term_curr;
}

} // namespace controller
} // namespace eymonttdev
