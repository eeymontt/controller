#include "controller_pid.h"

#include <iostream>
#include <cstdio>
#include <stdint.h>

namespace eymonttdev {
namespace controller {

PidController::PidController(
    PFunct& prop, IFunct& integ, DFunct& deriv, const GainManager& manager)
    : PidController(prop, integ, deriv, manager,
        settingsForPid_t
        {
            /* .direction */ PidDirection::Direct,
            /* .pErrorType */ PidErrorValueType::OnSetpoint,
            /* .dErrorType */ PidErrorValueType::OnSetpoint,
            /* .pErrorSquared */ false,
            /* .antiWindup */ PidAntiWindup::None,
            /* .integralMaxInstantError */ -1,
            /* .integralMaxValue */ -1,
            /* .zeroIntegralOnErrorSignChange */ true,
            /* .integralDeprecPercent */ 0.0f,
            /* .pTermWeight */ 1.0f,
            /* .dTermWeight */ 1.0f,
            /* .outputMax */ 1.0f,
            /* .outputMin */ 0.0f
        }
    )
{};

PidController::PidController(
    PFunct& prop, IFunct& integ, DFunct& deriv, const GainManager& manager,
    settingsForPid_t external_settings)
    : p_term_(prop)
    , i_term_(integ)
    , d_term_(deriv)
    , gains_(manager)
    , settings(external_settings)

    /* Non-supplied variables */
    , time_last_update_ms_{}
    , y_{0}
    , y_0{0}
    , r_{0}
    , error_for_p_{}
    , error_for_d_{}
{};

void PidController::update(uint16_t time_ms)
{
    const float delta_time_s = (time_ms - time_last_update_ms_)/1000.f;
    time_last_update_ms_ = time_ms;

    //printf("dt = %.8f second(s)\n", delta_time_s);
    //std::cout << std::fixed << delta_time_s << std::endl;

    /* Update system variables */

    y_.current = (*input);      // process variable (PV), a.k.a. controlled variable (CO)
    r_.current = (*setpoint);   // setpoint variable (SP)

    /* Update various errors */

    // I-term error
    sys_error_.current = r_.current - y_.current;

    // P-term error
    if (settings.pErrorType == PidErrorValueType::OnSetpoint)
    {
        error_for_p_.current = calc_error_(
            r_.current,
            y_.current,
            settings.pTermWeight
        );
    }
    else // PidErrorValueType::OnMeasurement
    {
        error_for_p_.current = calc_error_(
            y_.current,
            y_.previous,
            settings.pTermWeight
        );
    }

    // D-term error
    if (settings.dErrorType == PidErrorValueType::OnSetpoint)
    {
        error_for_d_.current = calc_error_(
            r_.current,
            y_.current,
            settings.dTermWeight
        );
    }
    else // PidErrorValueType::OnMeasurement
    {
        error_for_d_.current = calc_error_(
            y_.current,
            y_.previous,
            settings.dTermWeight
        );
    }

    /* Dynamically update system gains */

    pidGains_t gains;
    gains_.getGains(gains);

    p_term_.setControllerGain(gains.Kc);
    i_term_.setIntegralGains(gains.Kc, gains.Ti);
    d_term_.setDerivativeGains(gains.Kc, gains.Td);

    /* PID term calculations */

    p_term_.updateAction(error_for_p_, delta_time_s);
    d_term_.updateAction(error_for_d_, delta_time_s);

    /* Anti-windup */

    if (settings.zeroIntegralOnErrorSignChange
        && false) // @todo check signs, for now bypassed
    {
        i_term_.reassignAction(0);
    }

    float u; // output
    if (settings.antiWindup == PidAntiWindup::None)
    {
        i_term_.updateAction(sys_error_, delta_time_s);
    }
    else if (settings.antiWindup == PidAntiWindup::DisableAtSat)
    {
        // a type of clamping
        // requires `integralMaxValue` be valid
        i_term_.updateAction(sys_error_, delta_time_s);
        if (i_term_.action() > settings.integralMaxValue)
        {
            i_term_.reassignAction(settings.integralMaxValue);
        }
    }
    else if (settings.antiWindup == PidAntiWindup::BackCalc)
    {
        /*
        "The back-calculation anti-windup method uses a feedback loop to discharge the PID
        Controller's internal integrator when the controller hits specified saturation limits and
        enters nonlinear operation."
        */
        i_term_.updateAction(sys_error_, delta_time_s);
        float temp_sum = p_term_.action() + i_term_.action() - d_term_.action();

        if (temp_sum > 0 && temp_sum > settings.outputMax)
        {
            i_term_.reassignAction(
                settings.outputMax - p_term_.action() + d_term_.action()
            );
        }
        else if (temp_sum < 0 && temp_sum < settings.outputMin)
        {
            i_term_.reassignAction(
                settings.outputMin - p_term_.action() + d_term_.action()
            );
        }
    }

    // always apply large error conpensation when configured
    if (settings.integralMaxInstantError >= 0
        && sys_error_.current < settings.integralMaxInstantError)
    {
        i_term_.updateAction(sys_error_, delta_time_s);
    }

    // always apply integrator reduction when saturated
    float integ_action = i_term_.action();
    if (settings.integralMaxValue >= 0
        && integ_action > settings.integralMaxValue)
    {
        integ_action -= integ_action*settings.integralDeprecPercent;
        i_term_.reassignAction(integ_action);
    }

    /* Modify and set output */

    // PID output = PropAction{k} + IntegAction{k} + DerivAction{k}
    u = p_term_.action() + i_term_.action() + d_term_.action();

    u *= (settings.direction == PidDirection::Direct) ? 1 : -1;

    // limits are signed, so clamp after (potentially) negating output
    if (u > settings.outputMax) u = settings.outputMax;
    if (u < settings.outputMin) u = settings.outputMin;

    *output = u;

    // calculations now done, prep for next iteration

    y_.previous = y_.current;
    r_.previous = r_.current;

    sys_error_.previous = sys_error_.current;
    error_for_p_.previous = error_for_p_.current;
    error_for_d_.previous = error_for_d_.current;
}

bool PidController::reset()
{
    time_last_update_ms_ = 0;

    // @todo
    // @eeymontt
    // reset action terms appropriately, errors, adjust for setpoint reassignment, etc.

    return true;
}

} // namespace controller
} // namespace eymonttdev
