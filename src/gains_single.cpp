#include "gains_single.h"

namespace eymonttdev {
namespace controller {

void SingleGainManager::setGains(float Kc, float Ti, float Td)
{
    // When positive: assign value provided
    // When negative: assign value to zero

    gains_.Kc = (Kc > 0 ) ? Kc : 0.0f;
    gains_.Ti = (Ti > 0 ) ? Ti : 0.0f;
    gains_.Td = (Td > 0 ) ? Td : 0.0f;
}

void SingleGainManager::getGains(pidGains_t& gains) const
{
    gains.Kc = gains_.Kc;
    gains.Ti = gains_.Ti;
    gains.Td = gains_.Td;
}

} // namespace controller
} // namespace eymonttdev
