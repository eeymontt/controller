#include "proportional_direct.h"

namespace eymonttdev {
namespace controller {

void DirectProportional::updateAction(const controllerTerm_t& term, float dt_s)
{
    float action = calculate(K_, term.current, dt_s);
    prop_term_.previous = prop_term_.current;
    prop_term_.current = action;
}

float DirectProportional::calculate(float Kc, float term_curr, float dt)
{
    /*
    Direct proportional action is defined to be the controller gain multiplied
    by the current error term.
    
        PropAction{k} = Kc*x{k}
    */
    return Kc*term_curr;
}

} // namespace controller
} // namespace eymonttdev
