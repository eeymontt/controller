#include "gains_multiple.h"

namespace eymonttdev {
namespace controller {

void MultiGainManager::assignTable(pidTableRow_t *table, uint8_t numRows)
{
    if (table != nullptr)
    {
        _table = table;
        _numRows = numRows;
    }
}

void MultiGainManager::assignVelocityRef(float *vel)
{
    if (vel != nullptr) _vel = vel;
}

void MultiGainManager::getGains(pidGains_t &gains) const
{
    // De-reference velocity pointer
    float vel;
    vel = *_vel;

    getGainsAtVelocity_(gains, _table, _numRows, vel);
}

void MultiGainManager::getGainsAtVelocity_(
    pidGains_t &gains, pidTableRow_t *tab, unsigned int numRows, float vel) const
{
    //std::cout << "vel = " << vel << std::endl;

    if (vel < 0 || vel > tab[numRows - 1].maxVelocity)
    {
        gains.Kc = 0;
        gains.Ti = 0;
        gains.Td = 0;
        return;
    }

    for (auto i = 0; i < numRows; i++)
    {
        gains.Kc = tab[i].entry.Kc;
        gains.Ti = tab[i].entry.Ti;
        gains.Td = tab[i].entry.Td;
        if (tab[i].maxVelocity >= vel) break;
    }
}

} // namespace controller
} // namespace eymonttdev
